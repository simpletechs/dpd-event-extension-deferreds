(function(Script, when) {
    function deferrify(func) {
        return function() {
            var $def = when.defer(),
                args = Array.prototype.slice.call(arguments, 0),
                evtcb = args.pop(),
                cb = function(res, err) {
                    if(err) {
                        $def.reject(err);
                    } else {
                        $def.resolve(res);
                    }

                    return evtcb.apply(this, arguments);
                }
            args.push(cb);
            func.apply(this, args);

            return $def.promise;
        }
    }


    var _run = Script.prototype.run;
    Script.prototype.run = function(ctx, domain, fn) {
        if (typeof domain === 'object') {
            domain.$dpd = {};

            for(var resource in ctx.dpd) {
                if(ctx.dpd.hasOwnProperty(resource)) {
                    domain.$dpd[resource] = domain.$dpd[resource] || {};
                    for(var func in ctx.dpd[resource]) {
                        if(ctx.dpd[resource].hasOwnProperty(func) && ['get', 'post', 'put', 'del'].indexOf(func) != -1) {
                            domain.$dpd[resource][func] = ctx.dpd[resource]['$'+func] = deferrify(ctx.dpd[resource][func])
                        }
                    }
                }
            }
        }
        _run.call(this, ctx, domain, fn);
    }
})(require('deployd/lib/script'), require('when'));
